Source: aevol
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: David Parsons <david.parsons@inria.fr>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               zlib1g-dev,
               libx11-dev,
               libboost-dev,
               libboost-system-dev,
               libboost-filesystem-dev,
               mpi-default-dev,
               autoconf-archive
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/aevol
Vcs-Git: https://salsa.debian.org/med-team/aevol.git
Homepage: http://www.aevol.fr/
Rules-Requires-Root: no

Package: aevol
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: digital genetics model to run Evolution Experiments in silico
 Aevol is a digital genetics model: populations of digital organisms are
 subjected to a process of selection and variation, which creates a
 Darwinian dynamics.
 .
 By modifying the characteristics of selection (e.g. population size,
 type of environment, environmental variations) or variation (e.g.
 mutation rates, chromosomal rearrangement rates, types of
 rearrangements, horizontal transfer), one can study experimentally the
 impact of these parameters on the structure of the evolved organisms.
 In particular, since Aevol integrates a precise and realistic model of
 the genome, it allows for the study of structural variations of the
 genome (e.g. number of genes, synteny, proportion of coding sequences).
 .
 The simulation platform comes along with a set of tools for analysing
 phylogenies and measuring many characteristics of the organisms and
 populations along evolution.
